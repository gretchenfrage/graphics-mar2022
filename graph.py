import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime

# Option to toggle line connections
CONNECT_LINES = False
# Option to show both lines and points
SHOW_BOTH = True

AXIS_2 = True
AXIS_3 = False
AXIS_4 = False
AXIS_5 = True

# Load the data
df_client_received_pos = pd.read_csv('client-received-pos.csv', names=['timestamp', 'received'])
df_client_received_vel = pd.read_csv('client-received-vel.csv', names=['timestamp', 'received_vel'])
df_advance_client_log = pd.read_csv('advance-client-log.csv', names=['timestamp', 'tick', 'subtick_micros', 'total_micros'])
df_client_caught_up = pd.read_csv('client-caught-up.csv', names=['timestamp', 'predicted', 'predicted_vel', 'smoothed', 'smoothed_vel', 'tick', 'subtick_micros', 'total_micros'])
df_server = pd.read_csv('server-steve-log.csv', names=['timestamp', 'value', 'velocity', 'tick', 'subtick_micros', 'total_micros'])

# Convert UNIX microsecond timestamps to datetime
def convert_timestamp(df):
    df['timestamp'] = pd.to_datetime(df['timestamp'], unit='us')
convert_timestamp(df_client_received_pos)
convert_timestamp(df_client_received_vel)
convert_timestamp(df_advance_client_log)
convert_timestamp(df_client_caught_up)
convert_timestamp(df_server)

# Plotting
plt.figure(figsize=(14, 10))
ax1 = plt.gca()   # position axis
ax2 = ax1.twinx() # velocity axis
ax3 = ax1.twinx() # ticks axis
ax4 = ax1.twinx() # subtick micros axis
ax5 = ax1.twinx() # total micros axis

# Determine plot types based on flags
if CONNECT_LINES:
    plot_type = '-'
elif SHOW_BOTH:
    plot_type = '-o'
else:
    plot_type = 'o'

ax1.plot(df_client_received_pos['timestamp'], df_client_received_pos['received'], plot_type, label='Client Received', color='blue', linewidth=2)
ax1.plot(df_client_caught_up['timestamp'], df_client_caught_up['predicted'], plot_type, label='Client Predicted', color='green', linewidth=2)
ax1.plot(df_client_caught_up['timestamp'], df_client_caught_up['smoothed'], plot_type, label='Client Smoothed', color='orange', linewidth=2)
ax1.plot(df_server['timestamp'], df_server['value'], plot_type, label='Server', color='red', linewidth=2)

if AXIS_2:
    ax2.plot(df_client_received_vel['timestamp'], df_client_received_vel['received_vel'], plot_type, label='Client Received Vel', color='purple', linewidth=2)
    ax2.plot(df_client_caught_up['timestamp'], df_client_caught_up['predicted_vel'], plot_type, label='Client Predicted Vel', color='brown', linewidth=2)
    ax2.plot(df_client_caught_up['timestamp'], df_client_caught_up['smoothed_vel'], plot_type, label='Client Smoothed Vel', color='cyan', linewidth=2)
    ax2.plot(df_server['timestamp'], df_server['velocity'], plot_type, label='Server Vel', color='pink', linewidth=2)

if AXIS_3:
    ax3.plot(df_advance_client_log['timestamp'], df_advance_client_log['tick'], plot_type, label='Client Received Tick', color='black', linewidth=2)
    ax3.plot(df_client_caught_up['timestamp'], df_client_caught_up['tick'], plot_type, label='Client Caught Up Tick', color='gold', linewidth=2)
    ax3.plot(df_server['timestamp'], df_server['tick'], plot_type, label='Server Tick', color='magenta', linewidth=2)

if AXIS_4:
    ax4.plot(df_advance_client_log['timestamp'], df_advance_client_log['subtick_micros'], plot_type, label='Client Received Subtick Micros', color='teal', linewidth=2)
    ax4.plot(df_client_caught_up['timestamp'], df_client_caught_up['subtick_micros'], plot_type, label='Client Caught Up Subtick Micros', color='olive', linewidth=2)
    ax4.plot(df_server['timestamp'], df_server['subtick_micros'], plot_type, label='Server Subtick Micros', color='maroon', linewidth=2)

if AXIS_5:
    ax5.plot(df_advance_client_log['timestamp'], df_advance_client_log['total_micros'], plot_type, label='Client Received Total Micros', color='navy', linewidth=2)
    ax5.plot(df_client_caught_up['timestamp'], df_client_caught_up['total_micros'], plot_type, label='Client Caught Up Total Micros', color='lime', linewidth=2)
    ax5.plot(df_server['timestamp'], df_server['total_micros'], plot_type, label='Server Total Micros', color='steelblue', linewidth=2)


# Formatting the primary axis
ax1.set_xlabel('Time')
ax1.set_ylabel('Value')
ax1.set_title('Client and Server Values and Velocities Over Time')
ax1.legend(loc='upper left')
ax1.grid(True)

# Formatting the secondary axis
if AXIS_2:
    ax2.set_ylabel('Velocity')
    ax2.legend(loc='upper right')

if AXIS_3:
    ax3.set_ylabel('Ticks')
    ax3.legend(loc='upper right')

if AXIS_4:
    ax4.set_ylabel('Subtick Micros')
    ax4.legend(loc='lower right')

if AXIS_5:
    ax5.set_ylabel('Total Micros')
    ax5.legend(loc='lower right')

# Format x-axis with more readable date format
ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
ax1.xaxis.set_major_locator(mdates.MinuteLocator(interval=30))  # adjust the interval as needed
plt.gcf().autofmt_xdate()  # Rotation

# Show plot
plt.show()
