import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime

# Option to toggle line connections
CONNECT_LINES = False
# Option to show both lines and points
SHOW_BOTH = True

# Load the data
df_client = pd.read_csv('client-time-log.csv', names=['timestamp', 'tick', 'subtick_micros', 'total_micros'])
df_server = pd.read_csv('server-time-log.csv', names=['timestamp', 'tick', 'subtick_micros', 'total_micros'])

# Convert UNIX microsecond timestamps to datetime
df_client['timestamp'] = pd.to_datetime(df_client['timestamp'], unit='us')
df_server['timestamp'] = pd.to_datetime(df_server['timestamp'], unit='us')

# Plotting
plt.figure(figsize=(14, 10))
ax1 = plt.gca()  # primary axis
ax2 = ax1.twinx() # secondary axis
ax3 = ax1.twinx() # tertiary axis 

# Determine plot types based on flags
if CONNECT_LINES:
    plot_type = '-'
elif SHOW_BOTH:
    plot_type = '-o'
else:
    plot_type = 'o'

# Primary axis for values
ax1.plot(df_client['timestamp'], df_client['tick'], plot_type, label='Client Tick', linewidth=2, color='blue')
ax1.plot(df_server['timestamp'], df_server['tick'], plot_type, label='Server Tick', linewidth=2, color='green')
ax2.plot(df_client['timestamp'], df_client['subtick_micros'], plot_type, label='Client Subtick Micros', linewidth=2, color='purple')
ax2.plot(df_server['timestamp'], df_server['subtick_micros'], plot_type, label='Server Subtick Micros', linewidth=2, color='red')
ax3.plot(df_client['timestamp'], df_client['total_micros'], plot_type, label='Client Total Micros', linewidth=2, color='cyan')
ax3.plot(df_server['timestamp'], df_server['total_micros'], plot_type, label='Server Total Micros', linewidth=2, color='magenta')

# Formatting the primary axis
ax1.set_xlabel('Time')
ax1.set_ylabel('Tick')
#ax1.set_title('Client and Server Values and Velocities Over Time')
ax1.legend(loc='upper left')
ax1.grid(True)

# Formatting the secondary axis
ax2.set_ylabel('Subtick Micros')
ax2.legend(loc='upper right')

# Formatting the tertiary axis
ax3.set_ylabel('Total Micros')
ax3.legend(loc='lower right')

# Format x-axis with more readable date format
ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
ax1.xaxis.set_major_locator(mdates.MinuteLocator(interval=30))  # adjust the interval as needed
plt.gcf().autofmt_xdate()  # Rotation

# Show plot
plt.show()
