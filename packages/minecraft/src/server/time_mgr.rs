//! See `TimeMgr`.

use crate::{
    server::per_player::*,
    util_time::*,
    util_must_drain::MustDrain,
};
use std::{
    time::Instant,
    collections::VecDeque,
};
use anyhow::*;


const MAX_LAGGING_TICKS: u64 = 3;


/// Server-side manager for time.
pub struct TimeMgr {
    pub effects: VecDeque<TimeMgrEffect>,
    // current tick number
    tick: u64,
    // instant corresponding to the beginning of tick 0
    tick_zero: Instant,

    // per player, most-recently-received-from-client tick number it thought would be occurring
    // on server upon instant sent
    player_declared_tick: PerJoinedPlayer<u64>,
    // per player, most-recently-received-from-client microseconds into continuous tick logic it
    // thought server would be through (if fully simulated) upon instant sent (< 50,000)
    player_declared_subtick_micros: PerJoinedPlayer<u16>,

    // per player, current client-time tick number
    player_client_tick: PerJoinedPlayer<u64>,
    // per player, microseconds into client-time continuous tick logic simulated (< 50,000)
    player_client_subtick_micros: PerJoinedPlayer<u16>,
}

/// Effect flowing from the `TimeMgr` to the rest of the server.
#[derive(Debug)]
pub enum TimeMgrEffect {
    /// A tick has finished, and the next tick has been scheduled.
    TickDone {
        skip_next: u64,
    },
    /// A player's client-time has advanced.
    AdvanceClientTime {
        pk: JoinedPlayerKey,
        from_tick: u64,
        from_subtick_micros: u16,
        to_tick: u64,
        to_subtick_micros: u16,
    },
}

impl TimeMgr {
    /// Construct with tick number zero scheduled to start now.
    pub fn new() -> Self {
        TimeMgr {
            effects: Default::default(),
            tick: 0,
            tick_zero: Instant::now(),
            player_declared_tick: Default::default(),
            player_declared_subtick_micros: Default::default(),
            player_client_tick: Default::default(),
            player_client_subtick_micros: Default::default(),
        }
    }

    /// Current tick number.
    pub fn tick(&self) -> u64 {
        self.tick
    }

    /// Instant such that `tick_zero() + tick() * 50 milliseconds = tick_start()`.
    pub fn tick_zero(&self) -> Instant {
        self.tick_zero
    }

    /// Start instant of current tick.
    pub fn tick_start(&self) -> Instant {
        u32::try_from(self.tick).ok()
            .and_then(|tick| TICK.checked_mul(tick))
            .and_then(|duration| self.tick_zero.checked_add(duration))
            .expect("tick instant overflowed")
    }

    /// End instant of current tick.
    pub fn tick_end(&self) -> Instant {
        self.tick.checked_add(1)
            .and_then(|tick| u32::try_from(tick).ok())
            .and_then(|tick| TICK.checked_mul(tick))
            .and_then(|duration| self.tick_zero.checked_add(duration))
            .expect("tick instant overflowed")
    }

    /// Get player's current client-time tick number.
    pub fn client_tick(&self, pk: JoinedPlayerKey) -> u64 {
        self.player_client_tick[pk]
    }

    /// Get player's current client-time subtick micros.
    pub fn client_subtick_micros(&self, pk: JoinedPlayerKey) -> u16 {
        self.player_client_subtick_micros[pk]
    }

    /// Call upon player joining the game.
    pub fn join_player(&mut self, pk: JoinedPlayerKey) {
        let tick = self.tick;
        let subtick_micros = 0;

        self.player_declared_tick.insert(pk, tick);
        self.player_declared_subtick_micros.insert(pk, subtick_micros);
        self.player_client_tick.insert(pk, tick);
        self.player_client_subtick_micros.insert(pk, subtick_micros);
    }

    /// Call upon joined player being removed.
    pub fn remove_player(&mut self, pk: JoinedPlayerKey) {
        self.player_declared_tick.remove(pk);
        self.player_declared_subtick_micros.remove(pk);
        self.player_client_tick.remove(pk);
        self.player_client_subtick_micros.remove(pk);
    }

    /// Call when `tick_end()` is reached.
    pub fn on_tick_done(&mut self, players: &PlayerKeySpace) -> MustDrain {
        self.tick += 1;

        let now = Instant::now();
        let skip_next =
            if self.tick_end() < now {
                let behind_nanos = (now - self.tick_end()).as_nanos();
                // poor man's div_ceil
                let behind_ticks = match behind_nanos % TICK.as_nanos() {
                    0 => behind_nanos / TICK.as_nanos(),
                    _ => behind_nanos / TICK.as_nanos() + 1,
                };
                let behind_ticks = u32::try_from(behind_ticks).expect("time broke");
                warn!("running too slow, skipping {} ticks", behind_ticks);
                self.tick_zero += TICK * behind_ticks;
                behind_ticks as u64
            } else {
                0
            };

        self.effects.push_back(TimeMgrEffect::TickDone { skip_next });
        for pk in players.iter_joined() {
            let MustDrain = self.maybe_advance_client_time(pk);
        }
        MustDrain
    }

    /// Call upon receiving a declare client time message from a player.
    pub fn declare_client_time(
        &mut self,
        pk: JoinedPlayerKey,
        tick: u64,
        subtick_micros: u16,
    ) -> Result<MustDrain> {
        debug_assert!(subtick_micros < 50_000);
        self.player_declared_tick[pk] = tick;
        self.player_declared_subtick_micros[pk] = subtick_micros;
        Ok(self.maybe_advance_client_time(pk))
    }

    fn maybe_advance_client_time(&mut self, pk: JoinedPlayerKey) -> MustDrain {
        let from_tick = self.player_client_tick[pk];
        let from_subtick_micros = self.player_client_subtick_micros[pk];

        let mut to_tick = self.player_declared_tick[pk];
        let mut to_subtick_micros = self.player_declared_subtick_micros[pk];

        if (to_tick, to_subtick_micros) < (from_tick, from_subtick_micros) {
            to_tick = from_tick;
            to_subtick_micros = from_subtick_micros;
        }

        if to_tick < self.tick.saturating_sub(MAX_LAGGING_TICKS) {
            trace!("client time being dragged fowards");
            to_tick = self.tick - MAX_LAGGING_TICKS;
            to_subtick_micros = 0;
        } else if to_tick > self.tick {
            trace!("client time being held back");
            to_tick = self.tick;
            to_subtick_micros = 50_000;
        }

        if (from_tick, from_subtick_micros) != (to_tick, to_subtick_micros) {
            self.player_client_tick[pk] = to_tick;
            self.player_client_subtick_micros[pk] = to_subtick_micros;
            self.effects.push_back(TimeMgrEffect::AdvanceClientTime {
                pk, from_tick, from_subtick_micros, to_tick, to_subtick_micros,
            })
        }

        MustDrain
    }
}

impl Default for TimeMgr {
    fn default() -> Self {
        Self::new()
    }
}
