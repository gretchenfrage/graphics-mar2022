//! Game logic for advancing the world forward in time.

use crate::{
    server::{
        per_player::*,
        *,
    },
    sync_state_entities::*,
    util_time::*,
};


/// Do a tick of world simulation.
///
/// This does not include pieces of state subject to "client time".
///
/// For pieces of state which are subject to continuous client-side prediction (but not
/// client-time), one tick duration of continuous logic should be performed _before_ one occurrence
/// of discrete logic.
pub fn do_tick(server: &mut Server) {
    trace!("tick");


    let mut world = server.as_sync_world();
    
    let _ = world;
    let mut chunk_steves = world.chunk_steves.iter_move_batch();
    let mut chunk_pigs = world.chunk_pigs.iter_move_batch();

    //let (cc, ci) = world.sync_ctx.chunk_mgr.chunks().iter().next().unwrap();
    //chunk_steves.get(cc, ci);

    for (cc, ci) in world.sync_ctx.chunk_mgr.chunks().iter() {
        let mut steves = chunk_steves.get(cc, ci);
        while let Some(mut steve) = steves.next() {
            if steve.as_write().as_ref().state.client_time.is_some() { continue ; }

            let mut rel_pos = steve.as_write().as_ref().rel_pos;
            let mut vel = steve.as_write().as_ref().state.vel;
            
            //rel_pos += Vec3::from(0.05);
            /*
            vel -= GRAVITY_ACCEL * dt;
            do_physics(
                TICK.as_secs_f32(),
                &mut rel_pos,
                &mut vel,

            )*/
            /*sync_state_entities::do_steve_physics(
                TICK.as_secs_f32(),
                0.0,
                cc,
                &mut rel_pos,
                &mut vel,
                &world.getter,
                world.tile_blocks.as_ref(),
                &world.sync_ctx.game,
                Some(steve.as_write().extra_mut()),
            );*/
            sync_state_entities::steve_physics_continuous(
                TICK.as_secs_f32(),
                cc,
                &mut rel_pos,
                &mut vel,
                &world.getter,
                world.tile_blocks.as_ref(),
                &world.sync_ctx.game,
                Some(steve.as_write().extra_mut()),
                |_, _, _| (),
            );
            sync_state_entities::steve_physics_discrete(
                cc,
                &mut rel_pos,
                &mut vel,
                &world.getter,
                world.tile_blocks.as_ref(),
                &world.sync_ctx.game,
                Some(steve.as_write().extra_mut()),
            );


            let rel_cc_after = (rel_pos / CHUNK_EXTENT.map(|n| n as f32)).map(f32::floor);
            if rel_cc_after != Vec3::from(0.0) {
                let cc_after = cc + rel_cc_after.map(|n| n as i64);
                if world.getter.get(cc_after).is_none() {
                    // TODO: handle this better
                    continue;
                }
            }

            /*use std::io::Write as _;
            std::writeln!(
                &mut steve.as_write().extra_mut().file,
                "{}, {}, {}",
                std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),
                (cc.y * CHUNK_EXTENT.y) as f32 + rel_pos.y,
                vel.y,
            ).unwrap();*/

            steve.as_write().set_vel(vel);
            steve.set_rel_pos(rel_pos);
        }

        let mut pigs = chunk_pigs.get(cc, ci);
        while let Some(mut pig) = pigs.next() {
            let mut color = pig.as_write().as_ref().state.color;
            color += Rgb::from(0.05);
            color %= Rgb::from(1.0);
            //pig.set_color(color);
        }
    }
    
}

/// Advance all pieces of world state subject to the given player's "client time" by doing
/// `elapsed` seconds of continuous simulation for it.
pub fn advance_player_continuous(server: &mut Server, pk: JoinedPlayerKey, elapsed: f32) {
    let mut world = server.as_sync_world();
    let (
        _global_idx,
        etype,
        cc,
        ci,
        vector_idx,
    ) = world.sync_ctx.entities.borrow()
        .lookup(world.server_only.player_body_id[pk])
        //.expect("TODO");
        .unwrap_or_else(|| {
            println!("{:#?}", world.sync_ctx.entities);
            todo!()
        });
    assert_eq!(etype, EntityType::Steve);
    // TODO: don't require an iter move batch for this
    let mut imb = world.chunk_steves.iter_move_batch();
    let mut imc = imb.get(cc, ci);
    imc.skip(vector_idx);
    let mut body = imc.next().expect("TODO");
    //let mut pos = (cc * CHUNK_EXTENT).map(|n| n as f32) + body.as_write().as_ref().rel_pos;
    let mut rel_pos = body.as_write().as_ref().rel_pos;
    let mut vel = body.as_write().as_ref().state.vel;
    /*StevePhysics {
        getter: &world.getter,
        tile_blocks: world.tile_blocks.as_ref(),
        game: &world.sync_ctx.game,
    }.physics_continuous(elapsed, &mut pos, &mut vel);*/
    steve_physics_continuous(
        elapsed,
        cc,
        &mut rel_pos,
        &mut vel,
        &world.getter,
        world.tile_blocks.as_ref(),
        &world.sync_ctx.game,
        Some(body.as_write().extra_mut()),
        |_, _, _| (),
    );
    body.as_write().set_vel(vel);
    //body.set_rel_pos(pos - (cc * CHUNK_EXTENT).map(|n| n as f32));
    body.set_rel_pos(rel_pos);
}

/// Advance all pieces of world state subject to the given player's "client time" by doing one
/// occurrence of discrete simulation for it.
pub fn advance_player_discrete(server: &mut Server, pk: JoinedPlayerKey) {
    let mut world = server.as_sync_world();
    let (
        _global_idx,
        etype,
        cc,
        ci,
        vector_idx,
    ) = world.sync_ctx.entities.borrow()
        .lookup(world.server_only.player_body_id[pk])
        //.expect("TODO");
        .unwrap_or_else(|| {
            println!("{:#?}", world.sync_ctx.entities);
            todo!()
        });
    assert_eq!(etype, EntityType::Steve);
    // TODO: don't require an iter move batch for this
    let mut imb = world.chunk_steves.iter_move_batch();
    let mut imc = imb.get(cc, ci);
    imc.skip(vector_idx);
    let mut body = imc.next().expect("TODO");
    //let mut pos = (cc * CHUNK_EXTENT).map(|n| n as f32) + body.as_write().as_ref().rel_pos;
    let mut rel_pos = body.as_write().as_ref().rel_pos;
    let mut vel = body.as_write().as_ref().state.vel;
    /*StevePhysics {
        getter: &world.getter,
        tile_blocks: world.tile_blocks.as_ref(),
        game: &world.sync_ctx.game,
    }.physics_continuous(elapsed, &mut pos, &mut vel);*/
    steve_physics_discrete(
        cc,
        &mut rel_pos,
        &mut vel,
        &world.getter,
        world.tile_blocks.as_ref(),
        &world.sync_ctx.game,
        Some(body.as_write().extra_mut()),
    );
    body.as_write().set_vel(vel);
    //body.set_rel_pos(pos - (cc * CHUNK_EXTENT).map(|n| n as f32));
    body.set_rel_pos(rel_pos);

    /*
    let mut world = server.as_sync_world();
    let (
        _global_idx,
        etype,
        cc,
        ci,
        vector_idx,
    ) = world.sync_ctx.entities.borrow()
        .lookup(world.server_only.player_body_id[pk])
        .expect("TODO");
    assert_eq!(etype, EntityType::Steve);
    // TODO: don't require an iter move batch for this
    let mut imb = world.chunk_steves.iter_move_batch();
    let mut imc = imb.get(cc, ci);
    imc.skip(vector_idx);
    let mut body = imc.next().expect("TODO");
    let mut pos = (cc * CHUNK_EXTENT).map(|n| n as f32) + body.as_write().as_ref().rel_pos;
    let mut vel = body.as_write().as_ref().state.vel;
    StevePhysics {
        getter: &world.getter,
        tile_blocks: world.tile_blocks.as_ref(),
        game: &world.sync_ctx.game,
    }.physics_discrete(&mut pos, &mut vel);
    body.as_write().set_vel(vel);
    body.set_rel_pos(pos - (cc * CHUNK_EXTENT).map(|n| n as f32));
    */
}
