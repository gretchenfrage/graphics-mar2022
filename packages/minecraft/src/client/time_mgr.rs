//! See `TimeMgr`.

use crate::{
    client::per_player::*,
    util_time::*,
};
use std::time::{Instant, Duration};
use anyhow::*;


pub const MAX_CATCHUP: Duration = Duration::from_millis(200);


/// Client-side manager for time.
///
/// Preserves invariants that:
///
/// - Both `tick` and `tick + 1` are representable as `u64`.
/// - The end up the current tick is representable as `Instant`.
#[derive(Debug)]
pub struct TimeMgr {
    // TODOOOO
    pub caught_up_to: Option<Instant>,
    tick: u64,
    tick_zero: Instant,
    player_client_time: PerPlayer<TickSubticks>,
    player_client_branch_point: PerPlayer<u64>,
    catch_up: Option<TickSubticks>,
    player_client_catch_up: PerPlayer<Option<TickSubticks>>,
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
/*TODO not public*/pub struct TickSubticks {
    /*TODO not public*/pub tick: u64,
    /*TODO not public*/pub subtick_micros: u16,
}

impl TimeMgr {
    pub fn new(tick: u64, tick_zero: Instant) -> Result<Self> {
        tick_end_instant(tick, tick_zero)?;
        Ok(TimeMgr {
            caught_up_to: None,
            tick,
            tick_zero,
            player_client_time: Default::default(),
            player_client_branch_point: Default::default(),
            player_client_catch_up: Default::default(),
            catch_up: None,
        })
    }

    pub fn tick(&self) -> u64 {
        self.tick
    }

    pub fn tick_start(&self) -> Instant {
        tick_start_instant(self.tick, self.tick_zero).expect("unreachable")
    }

    pub fn tick_end(&self) -> Instant {
        tick_end_instant(self.tick, self.tick_zero).expect("unreachable")
    }

    pub fn client_branch_point(&self, pk: PlayerKey) -> u64 {
        self.player_client_branch_point[pk]
    }

    pub fn client_catch_up_tick(&self, pk: PlayerKey) -> u64 {
        // TODO better panic messages
        self.player_client_catch_up[pk].unwrap().tick
    }

    pub fn client_catch_up_subtick_micros(&self, pk: PlayerKey) -> u16 {
        self.player_client_catch_up[pk].unwrap().subtick_micros
    }

    pub fn add_player(
        &mut self,
        pk: PlayerKey,
        client_tick: u64,
        client_subtick_micros: u16,
    ) -> Result<()> {
        self.validate_client_time(client_tick, client_subtick_micros)?;
        let tst = TickSubticks { tick: client_tick, subtick_micros: client_subtick_micros };
        self.player_client_time.insert(pk, tst);
        self.player_client_branch_point.insert(pk, 1);
        self.player_client_catch_up.insert(pk, None);
        Ok(())
    }

    pub fn remove_player(&mut self, pk: PlayerKey) {
        self.player_client_time.remove(pk);
        self.player_client_branch_point.remove(pk);
        self.player_client_catch_up.remove(pk);
    }

    pub fn on_tick_done(&mut self, skip_next: u64, players: &PlayerKeySpace) -> Result<()> {
        self.tick += 1;
        tick_end_instant(self.tick, self.tick_zero)?;
        if skip_next > 0 {
            self.tick_zero = u32::try_from(skip_next).ok()
                .and_then(|i| TICK.checked_mul(i))
                .and_then(|d| self.tick_zero.checked_add(d))
                .ok_or_else(|| anyhow!("tick instant overflowed"))?;
        }
        self.reset_server_time();
        for pk in players.iter() {
            self.reset_client_time(pk);
        }
        Ok(())
    }

    pub fn on_post_tick_op_done(&mut self) {
        self.reset_server_time();
    }

    pub fn on_advance_client_done(
        &mut self,
        pk: PlayerKey,
        client_tick: u64,
        client_subtick_micros: u16,
    ) -> Result<()> {
        self.validate_client_time(client_tick, client_subtick_micros)?;
        let tst = TickSubticks { tick: client_tick, subtick_micros: client_subtick_micros };
        ensure!(
            tst >= self.player_client_time[pk],
            "client time ran backwards ({:?} -> {:?})",
            self.player_client_time[pk],
            tst,
        );
        self.player_client_time[pk] = tst;
        self.reset_client_time(pk);
        Ok(())
    }

    pub fn on_client_op_done(&mut self, pk: PlayerKey) {
        self.reset_client_time(pk);
    }

    fn validate_client_time(&self, tick: u64, subtick_micros: u16) -> Result<()> {
        debug_assert!(subtick_micros <= 50_000);
        ensure!(tick <= self.tick, "player tick {} exceeded server tick {}", tick, self.tick);
        Ok(())
    }

    fn reset_server_time(&mut self) {
        self.catch_up = None;
    }

    fn reset_client_time(&mut self, pk: PlayerKey) {
        self.player_client_branch_point[pk] = self.player_client_branch_point[pk].checked_add(1)
            .expect("client branch point overflowed");
        self.player_client_catch_up[pk] = None;
    }

    pub fn catch_up_plan(&self, now: Instant) -> AdvanceTimeOps {
        let [current, target] = self.catch_up_current_target(now);
        current.advance_ops_to(target)
    }

    pub fn catch_up_plan_client(&self, pk: PlayerKey, now: Instant) -> AdvanceTimeOps {
        let [current, target] = self.catch_up_current_target_client(pk, now);
        current.advance_ops_to(target)
    }

    pub fn client_catch_up_reset(&self, pk: PlayerKey) -> bool {
        self.player_client_catch_up[pk].is_none()
    }

    pub fn on_catch_up_done(&mut self, now: Instant, players: &PlayerKeySpace) {
        let [_, target] = self.catch_up_current_target(now);
        self.catch_up = Some(target);

        for pk in players.iter() {
            let [_, target] = self.catch_up_current_target_client(pk, now);
            self.player_client_catch_up[pk] = Some(target);
        }
    }

    fn catch_up_current_target(&self, now: Instant) -> [TickSubticks; 2] {
        let received = TickSubticks { tick: self.tick, subtick_micros: 0 };
        self.catch_up_current_target_inner(now, received, self.catch_up)
    }

    // TODO not public
    pub fn catch_up_current_target_client(&self, pk: PlayerKey, now: Instant) -> [TickSubticks; 2] {
        self.catch_up_current_target_inner(
            now,
            self.player_client_time[pk],
            self.player_client_catch_up[pk],
        )
    }

    fn catch_up_current_target_inner(
        &self,
        now: Instant,
        received: TickSubticks,
        catch_up: Option<TickSubticks>,
    ) -> [TickSubticks; 2] {
        let received_instant = tick_start_instant(received.tick, self.tick_zero).expect("TODO should we be catching this lol")
            + Duration::from_micros(received.subtick_micros as u64);
        //let target_instant = now.clamp(received_instant, received_instant.saturating_add(MAX_CATCHUP));
        let target_instant = now.clamp(received_instant, received_instant.checked_add(MAX_CATCHUP).unwrap() /* TODO AUGHHHHH WHATEVER */);
        let target_micros = target_instant.duration_since(self.tick_zero).as_micros();
        let target = u64::try_from(target_micros / 50_000)
            .map(|tick| TickSubticks {
                tick,
                subtick_micros: (target_micros % 50_000) as u16
            })
            .unwrap_or(TickSubticks { tick: u64::MAX, subtick_micros: 49_999 });
        let current = catch_up.unwrap_or(received);
        [current, target]
    }
    /*
    pub fn catch_up(&mut self, now: Instant) -> AdvanceTimeOps {
        let target = self.target_tst(now);
        let current = self.catch_up.unwrap_or(TickSubticks { tick: self.tick, subtick_micros: 0 });
        self.catch_up = Some(target);
        current.advance_ops_to(target)
    }

    pub fn client_catch_up(&mut self, pk: PlayerKey, now: Instant) -> AdvanceTimeOps {
        let target = self.target_tst(now)
    }

    fn now_to_tst(&self, now: Instant) -> TickSubticks {
        let tick_tst = TickSubticks { tick: self.tick, subticks: 0 };
        match now.cmp(self.tick_start) {
            Ordering::Equal => tick_tst,
            Ordering::Greater => now
                .duration_since(self.tick_start)
                .as_micros()
        }
    }
    */
    /*
    fn target_tst(&self, now: Instant) -> TickSubticks {
        // microsecs from tick_start to now, unless bounded
        let total_micros = now
            .max(self.tick_start) // lower bound 0
            .duration_since(self.tick_start)
            .min(MAX_CATCHUP) // upper bound MAX_CATCHUP
            .as_micros();
        // add to current tick unless overflows
        u64::try_from(self.tick as u128 + total_micros / 50_000)
            .map(|target_tick| TickSubticks {
                tick: target_tick,
                subtick_micros: (total_micros % 50_000) as u16, 
            })
            // saturate on overflow
            .unwrap_or(TickSubticks {
                tick: u64::MAX,
                subtick_micros: 50_000,
            })
    }*/
}

fn tick_start_instant(tick: u64, tick_zero: Instant) -> Result<Instant> {
    u32::try_from(tick).ok()
        .and_then(|tick| TICK.checked_mul(tick))
        .and_then(|duration| tick_zero.checked_add(duration))
        .ok_or_else(|| anyhow!("tick instant overflowed"))
}

fn tick_end_instant(tick: u64, tick_zero: Instant) -> Result<Instant> {
    tick.checked_add(1)
        .ok_or_else(|| anyhow!("tick number overflowed"))
        .and_then(|tick| tick_start_instant(tick, tick_zero))
}

impl TickSubticks {
    fn advance_ops_to(self, target: Self) -> AdvanceTimeOps {
        AdvanceTimeOps::new(self.tick, self.subtick_micros, target.tick, target.subtick_micros)
    }
}

/*
/// Client-side manager for time.
#[derive(Debug)]
pub struct TimeMgr {
    // tick number of next tick in terms of receiving tick completion from server
    tick: u64,
    // server-side scheduled instant of next tick in terms of receiving tick completion from server
    next_tick: Instant,
    /*// per player, "client-time" tracking state for that player
    player_client_time: PerPlayer<ClientTimeEntry>,*/
    player_client_tick: PerPlayer<u64>,
    player_client_subtick_micros: PerPlayer<u64>,
    player_client_branch_point: PerPlayer<u64>,
    player_client_catch_up: PerPlayer<Option<CatchUpState>>,
    // time tracking state for caught up predictions, if caught up predictions existing and valid
    catch_up: Option<CatchUpState>,
}

/*// "client-time" tracking state entry for a player
struct ClientTimeEntry {
    // tick number of next client-time tick in terms of receiving completion
    tick: u64,
    // server-side scheduled instant of next client-time tick in terms of receiving completion
    next_tick: Instant,
    // microseconds into continuous tick logic in terms of receiving sub-completion (< 50,000)
    subtick_micros: u16,
    // time tracking state for caught up predictions for player, if existing and valid
    catch_up: Option<CatchUpState>,
}*/

// time tracking state for caught up predictions, if caught up predictions existing and valid
#[derive(Debug, Copy, Clone)]
struct CatchUpState { // TODO: rename to TickSubtick and refactor maybe
    // tick number of next tick that will catch up to
    // guaranteed >= time_mgr.state
    tick: u64,
    /*// scheduled instant of next tick that will catch up to
    next_tick: Instant,*/
    // microseconds into continuous tick logic caught up to (< 50,000)
    subtick_micros: u16,
}
*//*
impl TimeMgr {
    pub fn new(
        tick: u64,
        next_tick: Instant,
        players: &PlayerKeySpace,
        player_client_time: &HashMap<DownPlayerIdx, FinalizeJoinGameClientTimeEntry>,
    ) -> Result<Self> {
        ensure!(players.len() == player_client_time.len(), "too many player_client_time entries");
        let player_client_time = players
            .try_new_per_player(|player_idx| {
                let pk = players.lookup(player_idx)?;
                let entry = player_client_time.get(pk)
                    .unwrap_or_else(|| anyhow!("pk missing from player_client_time"))?;
                ensure!(
                    entry.next_tick_num <= tick,
                    "player client time of a greater tick than server time",
                );
                let client_next_tick = u32::try_from(tick - entry.next_tick_num).ok()
                    .and_then(|x| TICK.checked_mul(x))
                    .and_then(|d| next_tick.checked_sub(d))
                    .ok_or_else(|| anyhow!("client time instant numeric overflow"))?;
                Ok(ClientTimeEntry {
                    tick: entry.tick,
                    next_tick: client_next_tick,
                    subtick_micros: entry.subtick_micros.into(),
                })
            })?;
        Ok(TimeMgr {
            tick,
            next_tick,
            player_client_time,
            catch_up: None,
        })
    }

    pub fn tick_num(&self) -> u64 {
        self.tick
    }

    pub fn client_branch_point(&self, pk: PlayerKey) -> u64 {
        self.player_client_branch_point[pk]
    }

    pub fn add_player(
        &mut self,
        pk: PlayerKey,
        client_tick: u64,
        client_subtick_micros: u16,
    ) -> Result<()> {
        debug_assert!(client_subtick_micros <= 50_000);
        ensure!(client_tick <= self.tick, "server initialized player tick beyond server tick");

        self.player_client_tick.insert(pk, client_tick);
        self.player_client_subtick_micros.insert(pk, client_subtick_micros);
        self.player_client_branch_point.insert(pk, 0);
        self.player_client_catch_up.insert(pk, None);
        Ok(())
    }

    pub fn remove_player(&mut self, pk: PlayerKey) {
        self.player_client_tick.remove(pk);
        self.player_client_subtick_micros.remove(pk);
        self.player_client_branch_point.remove(pk);
        self.player_client_catch_up.remove(pk);
    }

    pub fn on_tick_done(&mut self, skip_next: u64, players: &PlayerKeySpace) -> Result<()> {
        self.tick = self.tick.checked_add(1).ok_or_else(|| anyhow!("tick num overflowed"))?;
        self.next_tick = skip_next
            .checked_add(1)
            .and_then(|i| u32::try_from(i).ok())
            .and_then(|i| TICK.checked_mul(i))
            .and_then(|d| client.next_tick_instant.checked_add(d))
            .ok_or_else(|| anyhow!("tick instant overflowed"))?;
        self.catch_up = None;
        for pk in players.iter_joined() {
            self.player_client_catch_up[pk] = None;
        }
        Ok(())
    }

    pub fn on_client_advance_done(
        &mut self,
        pk: PlayerKey,
        next_tick_num: u64,
        subtick_micros: u16,
    ) -> Result<()> {
        debug_assert!(subtick_micros <= 50_000);
        ensure!(
            next_tick_num > self.player_client_tick[pk]
            || (next_tick_num == self.player_client_tick[pk]
                && subtick_micros > self.player_client_tick[pk]),
            "server advanced player time to a non-greater value",
        );
        ensure!(next_tick_num <= self.tick, "server advanced player tick beyond server tick");

        self.player_client_tick[pk] = next_tick_num;
        self.player_client_subtick_micros[pk] = subtick_micros;
        self.player_client_branch_point[pk] += 1;
        self.player_client_catch_up[pk] = None;
        Ok(())
    }

    pub fn on_client_op_done(&mut self, pk: PlayerKey) {
        self.player_client_branch_point[pk] += 1;
        self.player_client_catch_up[pk] = None;
    }

    pub fn catch_up_plan(&self, now: Instant) -> AdvanceTimeOps {
        // tick/subtick at received state
        let received = CatchUpState { tick: self.tick, subtick_micros: 0 };
        // tick/subtick at present
        // bridging instants (next_tick - TICK) -> (now)
        let target = received.catch_up_target(self.next_tick, now + TICK);
        // tick/subtick at caught up
        let current = self.catch_up.unwrap_or(received);
        current.catch_up_plan(target)
    }

    pub fn client_catch_up_plan(&self, pk: PlayerKey, now: Instant) -> AdvanceTimeOps {
        // tick/subtick at received client state
        let received = CatchUpState {
            tick: self.player_client_tick[pk],
            subtick_micros: self.player_client_subtick_micros[pk],
        };
        // tick/subtick at present
        // bridging instants 
    }

    /*
    pub fn catch_up_target(
        &self,
        mut now: Instant,
        players: &PlayerKeySpace,
    ) -> Option<CatchUpTarget> {
        if now + TICK < self.next_tick {
            now = self.next_tick - TICK;
        } else if now - MAX_CATCHUP > self.next_tick
    }
    
    pub fn catch_up_plan(&self, target: CatchUpTarget) -> (Option<u64>, AdvanceTimeOps) {



        /*
        let (curr_tick, curr_subtick_micros, reset) = match self.catch_up {
            Some(CatchUpState { tick, subtick_micros }) => (tick, subtick_micros, None),
            None => (self.tick, 0, Some(self.tick)),
        };


        let catch_up = self.catch_up.unwrap_or_else(|| CatchUpState {
            tick: self.tick,
            subtick_micros: self.subtick_micros,
        });
        if let Some(catch_up) = self.catch_up {

        }*/
    }*/
}

impl CatchUpState {
    // upholds guarantees:
    // - overflow resistant: no from/to values may trigger panics
    // - symmetrical over addition: only the difference between from and to matters
    fn catch_up_target(&self, from: Instant, to: Instant) -> Self {
        if to <= from {
            return *self;
        }
        let catchup_micros = (to - from).min(MAX_CATCHUP).as_micros();
        let current_total_micros = self.tick as u128 * 50_000 + self.subtick_micros as u128;
        let target_total_micros = current_total_micros + catchup_micros;
        u64::try_from(target_total_micros / 50_000)
            .map(|ticks| CatchUpState {
                ticks,
                subtick_micros: (target_total_micros % 50_000) as u16,
            })
            .unwrap_or_else(|_| {
                warn!("catch up target tick overflowed");
                CatchUpState {
                    ticks: u64::MAX,
                    subtick_micros: 50_000,
                }
            })
    }

    fn catch_up_plan(&self, target: Self) -> AdvanceTimeOps {
        AdvanceTimeOps::new(self.tick, self.subtick_micros, target.tick, target.subtick_micros)
    }
}
*/
/*
/// Client-side manager for time.
///
/// The network transport implementation handles forming a synchronized clock between the client
/// and the server. As such, we model the client and the server as both having an accurate and
/// commensurable ability to capture absolute instants in time, but also as having a positive and
/// perhaps dynamically varying latency for exchanging messages between the two.
///
/// Using this shared clock, the client and server commit to a pre-shared schedule for instants
/// at which ticks occur. A tick is a discrete game logic operation that occurs (starts) at that
/// instant in time, and each tick which occurs is assigned an integer tick number which increases
/// by 1 each time.
///
/// This tick schedule is only violated in the rare case that the server gets behind schedule and
/// thus intentionally skips some number of scheduled tick instants. Even when the server skips
/// ticks as such, it does not skip tick numbers.
///
/// On the server side, a tick is an atomic operation, and thus messages received from clients are
/// only ever processed before or after a given tick, not "within" that tick. The server-side tick
/// may trigger the sending of many edits to the client, which the client does not necessarily
/// receive and process all at once. As such, a client may experience events such as updates while
/// in a state of having applied some but not all edits resulting from a tick on the server.
///
/// When the server completes a tick, it sends all clients a message denoting that that tick was
/// just completed. As such, client events can be said to only ever occur before or after and not
/// within the receipt of the "completion" of that tick.
///
/// The client can be thought of as having various parts of it tracking two instants in time at a
/// given moment: the "caught up" instant, which corresponds to the actual present instant and
/// anticipated last finished tick number, and the "received" instant, which corresponds to the
/// instant represented by the last messages _received_ from the server and the last tick number
/// the client has received the finishing of from the server.
///
/// The "caught up" instant is called that because bits of client state which are subject to
/// client-side prediction may be simulated forward so that they are "caught up" from the received
/// instant to the caught up instant.
///
/// The server does not actually continuously tell the client what the current timestamp is at the
/// server directly, but rather, synchronizes the client's tick schedule to its own at
/// initialization time, and sends the client a message every time it completes a tick, which
/// also includes how many ticks will be skipped after. From this, the client can keep track of the
/// last received tick number and instant. It goes forward monotonically, or it is considered a
/// protocol violation.
///
/// The caught up instant is supposed to be kept equal to the actual current time at the client.
/// However, there may be some exceptions:
///
/// - The caught up instant shall not be before the received instant. As such, the caught up
///   instant bottoms out at the received instant if the current time is before the received
///   instant. This should only occur in cases of server and/or client clock inaccuracy.
/// - The caught up instant is prevented from exceeding the received instant by an excessively
///   large duration. As such, if the current time outpaces the received instant by too much,
///   the caught up instant may temporarily stop increasing. This should only occur in cases of
///   clock inaccuracy, excessively high latency, or temporary or permanent network disconnection
///   from the server.
///
/// The caught up tick number is currently not tracked or considered meaningful.
///
/// 
pub struct TimeMgr {

}
*/