
use crate::{
    client::{Client, Prediction},
    sync_state_entities::StevePhysics,
    util_time::*,
    message::PlayerAction,
};
use chunk_data::*;
use std::{
    time::Instant,
    collections::VecDeque,
};
use vek::*;


const SMOOTH_MIN_SPONTANEOUS_ACCEL: f32 = 8.0;
const SMOOTH_MAX_SPONTANEOUS_ACCEL: f32 = 2000.0;
const SMOOTH_MAX_POS_ERROR: f32 = 8.0;


pub fn catch_up(client: &mut Client, now: Instant) {
    // TODO
    let prev_caught_up_to = std::mem::replace(&mut client.pre_join.time_mgr.caught_up_to, Some(now));
    let elapsed = prev_caught_up_to.map(|pcup | /* haha pee cup */ now.duration_since(pcup).as_secs_f32()).unwrap_or(0.0);

    let getter = client.pre_join.chunks.getter();
    let mut physics = StevePhysics {
        getter: &getter,
        tile_blocks: &client.pre_join.tile_blocks,
        game: &client.pre_join.game,
    };
    for (cc, ci, _getter) in client.pre_join.chunks.iter() {
        for ent in client.pre_join.chunk_steves.get_mut(cc, ci) {
            // ==== determine catch up plan ====
            let pp = &mut ent.extra.prediction;

            let received_pos = (cc * CHUNK_EXTENT).map(|n| n as f32) + ent.entity.rel_pos;
            let received_vel = ent.entity.state.vel;
            if let &Some(ref username) = &ent.entity.state.client_time {
                let pk = client.pre_join.players.iter()
                    .find(|&pk| &client.pre_join.player_username[pk] == username) // TODO O(N)
                    .unwrap(); // TODO
                let ops = client.pre_join.time_mgr.catch_up_plan_client(pk, now);
                let bop = client.pre_join.time_mgr.client_branch_point(pk);
                let preds =
                    if pk == client.self_pk && client.pre_join.time_mgr.client_catch_up_reset(pk) {
                        Some(&client.pre_join.prediction_queue)
                    } else { None };
                pp.catch_up(&mut physics, bop, ops, preds, elapsed, received_pos, received_vel);
            } else {
                let ops = client.pre_join.time_mgr.catch_up_plan(now);
                let bop = client.pre_join.time_mgr.tick();
                pp.catch_up(&mut physics, bop, ops, None, elapsed, received_pos, received_vel);
            }

            /*
            let (ops, bop) = ent.entity.state.client_time.as_ref()
                .and_then(|username| client.pre_join.players.iter()
                    .find(|&pk| &client.pre_join.player_username[pk] == username)) // TODO O(N)
                .map(|pk| (
                    /*{
                        let [_, target] = client.pre_join.time_mgr.catch_up_current_target_client(pk, now);
                        use std::io::Write as _;
                        std::writeln!(
                            &mut client.pre_join.time_log,
                            "{}, {}, {}, {}",
                            std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),
                            target.tick,
                            target.subtick_micros,
                            target.tick as u128 * 50_000 + target.subtick_micros as u128,
                        ).unwrap();
                        client.pre_join.time_mgr.catch_up_plan_client(pk, now)
                    },*/
                    client.pre_join.time_mgr.catch_up_plan_client(pk, now),
                    client.pre_join.time_mgr.client_branch_point(pk),
                ))
                .unwrap_or_else(|| (
                    client.pre_join.time_mgr.catch_up_plan(now),
                    client.pre_join.time_mgr.tick(),
                ));

            // ==== deal with branch point / validate predicted ====
            if pp.branch_point > bop {
                if let Some(file) = ent.extra.caught_up_log.as_mut() {    let username = ent.entity.state.client_time.as_ref().unwrap();    let pk = client.pre_join.players.iter()        .find(|&pk| &client.pre_join.player_username[pk] == username)        .unwrap();    let [_, target] = client.pre_join.time_mgr.catch_up_current_target_client(pk, now);    use std::io::Write as _;    std::writeln!(        file,        "{}, {}, {}, {}, {}, {}, {}, {}",        std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),        ent.extra.prediction.predicted_pos.y,        ent.extra.prediction.predicted_vel.y,        ent.extra.prediction.smoothed_pos.y,        ent.extra.prediction.smoothed_vel.y,        target.tick,        target.subtick_micros,        target.tick as u128 * 50_000 + target.subtick_micros as u128,    ).unwrap();}
                continue;
            } else if pp.branch_point < bop {
                pp.branch_point = bop;
                pp.predicted_pos = (cc * CHUNK_EXTENT).map(|n| n as f32) + ent.entity.rel_pos;
                pp.predicted_vel = ent.entity.state.vel;
            }

            // ==== catch up ====
            for op in ops {
                match op {
                    AdvanceTimeOp::Continuous(dt) => physics
                        .physics_continuous(dt, &mut pp.predicted_pos, &mut pp.predicted_vel),
                    AdvanceTimeOp::Discrete => physics
                        .physics_discrete(&mut pp.predicted_pos, &mut pp.predicted_vel),
                }
            }

            // ==== smooth ====
            if !pp.smoothed {
                // initialize smoothed from invalid if necessary
                pp.smoothed = true;
                pp.smoothed_pos = pp.predicted_pos;
                pp.smoothed_vel = pp.predicted_vel;
                //if let Some(file) = ent.extra.caught_up_log.as_mut() {    let username = ent.entity.state.client_time.as_ref().unwrap();    let pk = client.pre_join.players.iter()        .find(|&pk| &client.pre_join.player_username[pk] == username)        .unwrap();    let [_, target] = client.pre_join.time_mgr.catch_up_current_target_client(pk, now);    use std::io::Write as _;    std::writeln!(        file,        "{}, {}, {}, {}, {}, {}, {}, {}",        std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),        ent.extra.prediction.predicted_pos.y,        ent.extra.prediction.predicted_vel.y,        ent.extra.prediction.smoothed_pos.y,        ent.extra.prediction.smoothed_vel.y,        target.tick,        target.subtick_micros,        target.tick as u128 * 50_000 + target.subtick_micros as u128,    ).unwrap();}
                continue;
            }

            // otherwise, update smoothed continuously

            let pos_err = pp.predicted_pos - pp.smoothed_pos;
            if pos_err == Vec3::from(0.0) {
                // no error edge case
                pp.smoothed_vel = pp.predicted_vel;
                //if let Some(file) = ent.extra.caught_up_log.as_mut() {    let username = ent.entity.state.client_time.as_ref().unwrap();    let pk = client.pre_join.players.iter()        .find(|&pk| &client.pre_join.player_username[pk] == username)        .unwrap();    let [_, target] = client.pre_join.time_mgr.catch_up_current_target_client(pk, now);    use std::io::Write as _;    std::writeln!(        file,        "{}, {}, {}, {}, {}, {}, {}, {}",        std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),        ent.extra.prediction.predicted_pos.y,        ent.extra.prediction.predicted_vel.y,        ent.extra.prediction.smoothed_pos.y,        ent.extra.prediction.smoothed_vel.y,        target.tick,        target.subtick_micros,        target.tick as u128 * 50_000 + target.subtick_micros as u128,    ).unwrap();}
                continue;
            }

            // force smooth vel into the direction of pos error
            // allowing it to keep its existing smooth vel or match that of predicted vel 
            let pred_dot = pp.predicted_vel.dot(pos_err);
            let smoo_dot = pp.smoothed_vel.dot(pos_err);

            pp.smoothed_vel =
                if pred_dot <= 0.0 && smoo_dot <= 0.0 {
                    Vec3::from(0.0)
                } else {
                    pos_err * (smoo_dot.max(pred_dot) / pos_err.magnitude_squared())
                };

            

            let spon_accel_fract =
                pos_err.magnitude() / SMOOTH_MAX_POS_ERROR;

            // let it teleport if it's too far away
            if spon_accel_fract > 1.0 {
                pp.smoothed_pos = pp.predicted_pos;
                pp.smoothed_vel = pp.predicted_vel;
                //if let Some(file) = ent.extra.caught_up_log.as_mut() {    let username = ent.entity.state.client_time.as_ref().unwrap();    let pk = client.pre_join.players.iter()        .find(|&pk| &client.pre_join.player_username[pk] == username)        .unwrap();    let [_, target] = client.pre_join.time_mgr.catch_up_current_target_client(pk, now);    use std::io::Write as _;    std::writeln!(        file,        "{}, {}, {}, {}, {}, {}, {}, {}",        std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),        ent.extra.prediction.predicted_pos.y,        ent.extra.prediction.predicted_vel.y,        ent.extra.prediction.smoothed_pos.y,        ent.extra.prediction.smoothed_vel.y,        target.tick,        target.subtick_micros,        target.tick as u128 * 50_000 + target.subtick_micros as u128,    ).unwrap();}
                continue;
            }

            // let it spontaneously accelerate
            let spon_accel =
                SMOOTH_MIN_SPONTANEOUS_ACCEL
                + (SMOOTH_MAX_SPONTANEOUS_ACCEL - SMOOTH_MIN_SPONTANEOUS_ACCEL) * spon_accel_fract;
            pp.smoothed_vel += pp.smoothed_vel.normalized() * spon_accel * elapsed;

            // use vel to modify position
            let delta_smoo_pos = pp.smoothed_vel * elapsed;
            if delta_smoo_pos.magnitude_squared() < pos_err.magnitude_squared() {
                pp.smoothed_pos += delta_smoo_pos;
            } else {
                // case where catches up
                pp.smoothed_pos = pp.predicted_pos;
                pp.smoothed_vel = Vec3::from(0.0);
            }

            /*if let Some(file) = ent.extra.caught_up_log.as_mut() {
                let username = ent.entity.state.client_time.as_ref().unwrap();
                let pk = client.pre_join.players.iter()
                    .find(|&pk| &client.pre_join.player_username[pk] == username)
                    .unwrap();
                let [_, target] = client.pre_join.time_mgr.catch_up_current_target_client(pk, now);
                use std::io::Write as _;
                std::writeln!(
                    file,
                    "{}, {}, {}, {}, {}, {}, {}, {}",
                    std::time::SystemTime::now().duration_since(std::time::SystemTime::UNIX_EPOCH).unwrap().as_micros(),
                    ent.extra.prediction.predicted_pos.y,
                    ent.extra.prediction.predicted_vel.y,
                    ent.extra.prediction.smoothed_pos.y,
                    ent.extra.prediction.smoothed_vel.y,
                    target.tick,
                    target.subtick_micros,
                    target.tick as u128 * 50_000 + target.subtick_micros as u128,
                ).unwrap();
            }*/*/
        }
    }
    client.pre_join.time_mgr.on_catch_up_done(now, &client.pre_join.players);
}

#[derive(Debug, Copy, Clone, Default)]
pub struct PredictionParticle {
    pub branch_point: u64, // TODO: initialize future BOP properly
    pub predicted_pos: Vec3<f32>,
    pub predicted_vel: Vec3<f32>,
    pub smoothed: bool,
    pub smoothed_pos: Vec3<f32>,
    pub smoothed_vel: Vec3<f32>,
}

impl PredictionParticle {
    fn catch_up(
        &mut self,
        physics: &mut StevePhysics,
        bop: u64,
        ops: AdvanceTimeOps,
        preds: Option<&VecDeque<Prediction>>,
        elapsed: f32,
        received_pos: Vec3<f32>,
        received_vel: Vec3<f32>,
    ) {
        // ==== deal with branch point / validate predicted ====
        if self.branch_point > bop {
            return;
        } else if self.branch_point < bop {
            self.branch_point = bop;
            self.predicted_pos = received_pos;
            self.predicted_vel = received_vel;
        }

        // ==== catch up ====
        if let Some(preds) = preds {
            self.catch_up_inner(
                physics,
                ops.with_events(preds.iter()
                    .map(|pred| (pred.tick, pred.subtick_micros, &pred.action))),
            );
        } else {
            self.catch_up_inner(physics, ops.map(From::from));
        }

        // ==== smooth ====
        if !self.smoothed {
            // initialize smoothed from invalid if necessary
            self.smoothed = true;
            self.smoothed_pos = self.predicted_pos;
            self.smoothed_vel = self.predicted_vel;
            return;
        }

        // otherwise, update smoothed continuously

        let pos_err = self.predicted_pos - self.smoothed_pos;
        if pos_err == Vec3::from(0.0) {
            // no error edge case
            self.smoothed_vel = self.predicted_vel;
            return;
        }

        // force smooth vel into the direction of pos error
        // allowing it to keep its existing smooth vel or match that of predicted vel 
        let pred_dot = self.predicted_vel.dot(pos_err);
        let smoo_dot = self.smoothed_vel.dot(pos_err);

        self.smoothed_vel =
            if pred_dot <= 0.0 && smoo_dot <= 0.0 {
                Vec3::from(0.0)
            } else {
                pos_err * (smoo_dot.max(pred_dot) / pos_err.magnitude_squared())
            };        

        let spon_accel_fract =
            pos_err.magnitude() / SMOOTH_MAX_POS_ERROR;

        // let it teleport if it's too far away
        if spon_accel_fract > 1.0 {
            self.smoothed_pos = self.predicted_pos;
            self.smoothed_vel = self.predicted_vel;
            return;
        }

        // let it spontaneously accelerate
        let spon_accel =
            SMOOTH_MIN_SPONTANEOUS_ACCEL
            + (SMOOTH_MAX_SPONTANEOUS_ACCEL - SMOOTH_MIN_SPONTANEOUS_ACCEL) * spon_accel_fract;
        self.smoothed_vel += self.smoothed_vel.normalized() * spon_accel * elapsed;

        // use vel to modify position
        let delta_smoo_pos = self.smoothed_vel * elapsed;
        if delta_smoo_pos.magnitude_squared() < pos_err.magnitude_squared() {
            self.smoothed_pos += delta_smoo_pos;
        } else {
            // case where catches up
            self.smoothed_pos = self.predicted_pos;
            self.smoothed_vel = Vec3::from(0.0);
        }
    }

    fn catch_up_inner<'a, I: Iterator<Item=AdvanceTimeWithEventsOp<&'a PlayerAction>>>(
        &mut self,
        physics: &mut StevePhysics,
        ops: I,
    ) {
        for op in ops {
            match op {
                AdvanceTimeWithEventsOp::Continuous(dt) => physics
                    .physics_continuous(dt, &mut self.predicted_pos, &mut self.predicted_vel),
                AdvanceTimeWithEventsOp::Discrete => physics
                    .physics_discrete(&mut self.predicted_pos, &mut self.predicted_vel),
                AdvanceTimeWithEventsOp::Event(pred) => {
                    self.predicted_vel.y += 30.0;
                }
            }
        }
    }
}
