//! Do a tick of physics to a physics object.

use super::{
    collision::CollisionObject,
    world_geometry::WorldGeometry,
};
use chunk_data::Face;
use vek::*;


/// Do a tick of physics to a physics object.
///
/// At the end of each linear segment the object travels through, `linear_path_visitor` is
/// called with:
///
/// - The _remaining_ dt at the end of the segment.
/// - The pos of the object at the end of that segment.
/// - The velocity that the object moved with _throughout_ that segment.
pub fn do_physics<C, W, L>(
    mut dt: f32,
    pos: &mut Vec3<f32>,
    vel: &mut Vec3<f32>,
    collision_obj: &C,
    world_geom: &W,
    mut linear_path_visitor: L,
) -> DidPhysics<W::BarrierId>
where
    C: CollisionObject,
    W: WorldGeometry,
    L: FnMut(f32, Vec3<f32>, Vec3<f32>),
{
    const EPSILON: f32 = 0.0001;

    let mut on_ground = None;

    while dt > EPSILON {
        let start_vel = *vel;

        if let Some(collision) = collision_obj.first_collision(
            -EPSILON,
            dt,
            *pos,
            *vel,
            world_geom,
        ) {
            if collision.barrier_face == Face::PosY {
                on_ground = Some(collision.barrier_id);
            }

            *pos += *vel * collision.dt;
            pos[collision.barrier_face.to_axis() as usize] +=
                EPSILON * collision.barrier_face.to_pole().to_int() as f32;
            vel[collision.barrier_face.to_axis() as usize] = 0.0;
            if collision.dt > 0.0 {
                dt -= collision.dt;
            }
        } else {
            *pos += *vel * dt;
            dt = 0.0;
        }

        linear_path_visitor(dt, *pos, start_vel);
    }

    DidPhysics {
        on_ground,
    }
}

/// Information returned from `do_physics`.
#[derive(Debug, Clone)]
pub struct DidPhysics<B> {
    /// Whether physics object collided with the ground at all.
    pub on_ground: Option<B>,
}
