//! Time handling utilities.

use crate::game_binschema::*;
use std::{
    time::{Instant, Duration},
    iter::Peekable,
};


/// Duration of a tick.
pub const TICK: Duration = Duration::from_millis(50);


/// Microsecond-precision timestamp relative to a known or estimated `server_t0` instant of a
/// particular server/client connection.
///
/// Suitable for transmission across that connection to convey real time instants without relying
/// on either side having accurately synchronized Unix clocks.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, GameBinschema)]
pub struct ServerRelTime(pub i64);

impl ServerRelTime {
    /// Represent an instant relative to `server_t0`.
    ///
    /// Warns and saturates if called with instants ridiculously far before or after `server_t0`.
    pub fn new(instant: Instant, server_t0: Instant) -> Self {
        ServerRelTime(if instant >= server_t0 {
            i64::try_from(instant.duration_since(server_t0).as_micros()).ok()
                .unwrap_or_else(|| {
                    warn!("time_relativize called with timestamp ridiculously far into future");
                    i64::MAX
                })
        } else {
            i64::try_from(server_t0.duration_since(instant).as_micros()).ok()
                .unwrap_or_else(|| {
                    warn!("time_relativize called with timestamp ridiculously far into past");
                    i64::MIN
                })
                .saturating_neg()
        })
    }

    /// Convert to an instant relative to `server_t0`.
    pub fn to_instant(self, server_t0: Instant) -> Instant {
        if self.0 >= 0 {
            server_t0 + Duration::from_micros(self.0 as u64)
        } else {
            server_t0 - Duration::from_micros(-self.0 as u64)
        }
    }
}


const MICROS_PER_TICK: u16 = 50_000;
const MICRO: f32 = 1.0 / 1_000_000.0;

#[derive(Debug, Clone)]
pub struct AdvanceTimeOps {
    curr_tick: u64,
    curr_subtick_micros: u16,
    target_tick: u64,
    target_subtick_micros: u16,
}

#[derive(Debug, Copy, Clone)]
pub enum AdvanceTimeOp {
    Continuous(f32),
    Discrete,
}

impl AdvanceTimeOps {
    pub fn new(
        curr_tick: u64,
        curr_subtick_micros: u16,
        target_tick: u64,
        target_subtick_micros: u16,
    ) -> Self {
        debug_assert!(
            (target_tick, target_subtick_micros) >= (curr_tick, curr_subtick_micros)
        );
        AdvanceTimeOps {
            curr_tick,
            curr_subtick_micros,
            target_tick,
            target_subtick_micros,
        }
    }

    pub fn empty() -> Self {
        Self::new(0, 0, 0, 0)
    }

    pub fn with_events<I, E>(&self, events: I) -> AdvanceTimeWithEventsOps<I::IntoIter>
    where
        I: IntoIterator<Item=(u64, u16, E)>,
    {
        AdvanceTimeWithEventsOps {
            curr_tick: self.curr_tick,
            curr_subtick_micros: self.curr_subtick_micros,
            target_tick: self.target_tick,
            target_subtick_micros: self.target_subtick_micros,
            events: events.into_iter().peekable(),
        }
    }
}

impl Iterator for AdvanceTimeOps {
    type Item = AdvanceTimeOp;

    fn next(&mut self) -> Option<AdvanceTimeOp> {
        if self.curr_tick == self.target_tick {
            if self.curr_subtick_micros == self.target_subtick_micros {
                None
            } else {
                let microsecs = self.target_subtick_micros - self.curr_subtick_micros;
                self.curr_subtick_micros = self.target_subtick_micros;
                Some(AdvanceTimeOp::Continuous(microsecs as f32 * MICRO))
            }
        } else {
            if self.curr_subtick_micros < MICROS_PER_TICK {
                let microsecs = MICROS_PER_TICK - self.curr_subtick_micros;
                self.curr_subtick_micros = MICROS_PER_TICK;
                Some(AdvanceTimeOp::Continuous(microsecs as f32 * MICRO))
            } else {
                self.curr_subtick_micros = 0;
                self.curr_tick += 1;
                Some(AdvanceTimeOp::Discrete)
            }
        }
    }
}

pub struct AdvanceTimeWithEventsOps<I: Iterator> {
    curr_tick: u64,
    curr_subtick_micros: u16,
    target_tick: u64,
    target_subtick_micros: u16,
    events: Peekable<I>,
}

#[derive(Debug, Copy, Clone)]
pub enum AdvanceTimeWithEventsOp<E> {
    Continuous(f32),
    Discrete,
    Event(E),
}

impl<I: Iterator<Item=(u64, u16, E)>, E> Iterator for AdvanceTimeWithEventsOps<I> {
    type Item = AdvanceTimeWithEventsOp<E>;

    fn next(&mut self) -> Option<AdvanceTimeWithEventsOp<E>> {
        if let Some((_, _, event)) = self.events
            .next_if(|&(event_tick, event_subtick_micros, _)|
                (event_tick, event_subtick_micros) <= (self.curr_tick, self.curr_subtick_micros)
            )
        {
            // event
            Some(AdvanceTimeWithEventsOp::Event(event))
        } else if self.curr_tick >= self.target_tick {
            if self.curr_subtick_micros >= self.target_subtick_micros {
                // done
                None
            } else {
                // subticks of last tick
                let next_subtick_micros = self.events.peek()
                    .filter(|&&(event_tick, event_subtick_micros, _)|
                        event_tick == self.target_tick
                        && event_subtick_micros < self.target_subtick_micros
                    )
                    .map(|&(_, event_subtick_micros, _)| event_subtick_micros.max(self.curr_subtick_micros))
                    .unwrap_or(self.target_subtick_micros);

                let microsecs = next_subtick_micros - self.curr_subtick_micros;
                self.curr_subtick_micros = next_subtick_micros;
                Some(AdvanceTimeWithEventsOp::Continuous(microsecs as f32 * MICRO))
            }
        } else {
            if self.curr_subtick_micros < MICROS_PER_TICK {
                // subticks of non-last tick
                let next_subtick_micros = self.events.peek()
                    .filter(|&&(event_tick, _, _)| event_tick == self.curr_tick)
                    .map(|&(_, event_subtick_micros, _)| event_subtick_micros.max(self.curr_subtick_micros))
                    .unwrap_or(MICROS_PER_TICK);

                let microsecs = next_subtick_micros - self.curr_subtick_micros;
                self.curr_subtick_micros = next_subtick_micros;
                Some(AdvanceTimeWithEventsOp::Continuous(microsecs as f32 * MICRO))
            } else {
                // advance to next tick
                self.curr_subtick_micros = 0;
                self.curr_tick += 1;
                Some(AdvanceTimeWithEventsOp::Discrete)
            }
        }
    }
}

impl<E> From<AdvanceTimeOp> for AdvanceTimeWithEventsOp<E> {
    fn from(op: AdvanceTimeOp) -> Self {
        match op {
            AdvanceTimeOp::Continuous(elapsed) => AdvanceTimeWithEventsOp::Continuous(elapsed),
            AdvanceTimeOp::Discrete => AdvanceTimeWithEventsOp::Discrete,
        }
    }
}
